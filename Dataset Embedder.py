#!/usr/bin/env python
# coding: utf-8

# In[62]:


import glob
from PIL import Image
import os
import numpy as np
from sklearn import preprocessing
from collections import Counter
import tensorflow as tf
from tensorflow.python.platform import gfile
import argparse
import time
import pickle

def fixed_standardize(image):

    std_image = (image - 127.5) * 2
    
    return std_image

def prewhiten(image):
    
    mean = np.mean(image)
    std = np.std(image)
    std_adj = np.maximum(std, 1.0/np.sqrt(image.size))
    whitened_image = np.multiply(np.subtract(image, mean), 1/std_adj)
    
    return whitened_image

def main():
    
    """
        INPUTS (OPTIONAL ARGUMENTS MARKED '*'):
        
         1    dataset_name: String (e.g. 'CASIA')
         2    dataset_path: String (path to root dataset directory)
         3      model_name: String (e.g. 'InceptionV1')
         4         pb_file: String (path to CNN protobuf)
        *5       min_count: int (minimum images per subject; default = 10)
        *6       prewhiten: Boolean (default = False)
        *7       normalize: Boolean (default = False)
        *8      input_name: String (name of input logit)
        *9     output_name: String (name of output logit)
                
        OUTPUTS:
        
             Pickled[embeddings, labels]: saved as "Embeddings/{DATASET NAME}_{MODEL_NAME}.pkl"          
    """
    """
    parser = ArgumentParser(description = 'Face Recognition Evaluator')
    
    parser.add_argument('dataset_name', help = 'dataset identifier name')
    parser.add_argument('dataset_path', help = 'dataset location path')
    parser.add_argument('model_name', help = 'CNN model identifier name')
    parser.add_argument('pb_file', help = 'CNN model protobuf location path')
    parser.add_argument('min_count', help = 'minimum number of images per subject', type = int)
    parser.add_argument('prewhiten', help = 'applies prewhitening to images')
    parser.add_argument('normalize', help = 'normalizes images')
    parser.add_argument('--input_name', help = 'CNN model input tensor name (default = import/input)')
    parser.add_argument('--output_name', help = 'CNN model output tensor name (default = import/embeddings)')
    
    args = parser.parse_args()
    
    pp_config = [args.prewhiten, args.normalize]
    """
    
    dataset_name = 'CASIA'
    dataset_file = '/Volumes/NTFS Partition/Datasets/Face Rec/CASIA/MTCNN_160'
    model_name = 'InceptionV1'
    pb_file = 'Models/20180402-114759.pb'
    min_count = 10
    pp_config = [True, True]
    input_name = 'import/input'
    output_name = 'import/embeddings'


    # 1. LOAD GRAPH

    start = time.time()
    
    with gfile.FastGFile(pb_file, 'rb') as f:
        graph_def = tf.GraphDef()
        graph_def.ParseFromString(f.read())
        
    with tf.Graph().as_default() as graph:
        tf.import_graph_def(graph_def)
        
    stop = time.time()
    
    print("LOADED FROZEN GRAPH '" + pb_file + "': " + str(stop - start)[:7] + " s")


    # 2. CREATE EMBEDDINGS

    embeddings, labels, filenames = [], [], []

    with tf.Session(graph = graph) as sess:
        
        x = graph.get_tensor_by_name(input_name + ':0')
        pt = graph.get_tensor_by_name('import/phase_train:0')
        y = graph.get_tensor_by_name(output_name + ':0')

        folders = os.listdir(dataset_file)[1:-3]
        batch, n_batches, batch_size, n_images = [], 0, 5000or r, 0

        #i, c = 0, False

        for folder in folders:

            #if c:
            #    break
        
            files = os.listdir(dataset_file + '/' + folder)
        
            if len(files) < min_count:
                continue
            
            for file in files:

                #if i > 100:
                #    c = True
                #    break

                #i += 1

                if len(batch) >= batch_size:
                    save_location = "Embeddings/" + model_name + "/" + dataset_name + "_" + model_name + "_" + str(n_batches) + ".pkl"
                    pickle.dump((np.array(batch), np.array(labels), np.array(filenames)), open(save_location, 'wb'))
                    print("SAVED BATCH: " + save_location)
                    batch, labels, filenames = [], [], []
                    n_batches += 1
                                    
                image_path = dataset_file + '/' + folder + '/' + file
                filenames.append(image_path)

                start1 = time.time()
                image = Image.open(image_path)
                stop1 = time.time()
                reading_time = str(stop1 - start1)[:7]

                image_arr = prewhiten(np.array(image))

                im_list = []
                im_list.append(image_arr)
                im_list = np.array(im_list)

                start2 = time.time()
                embedding = sess.run(y, feed_dict = { x:im_list, pt:False })
                stop2 = time.time()
                embedding_time = str(stop2 - start2)[:7]

                label = int(folder)
            
                #embeddings.append(embedding.flatten())
                batch.append(embedding.flatten())
                labels.append(label) 

                n_images += 1

                print("# OF IMAGES PROCESSED: " + str(n_images) + "; read = " + reading_time + "; embed = " + embedding_time, end = '\r')

    
    # 3. SAVE IMAGE EMBEDDINGS & LABELS

    """
    
    save_location = "Embeddings/" + dataset_name + "_" + model_name + ".pkl"
    pickle.dump((np.array(embeddings), np.array(labels), np.array(filenames)), open(save_location, 'wb'))
    
    print("\t3.0: PICKLED AND SAVED EMBEDDINGS + LABELS: " + save_location)

    """


if __name__ == '__main__':
    main()

